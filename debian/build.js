/* This file is inspired by debian/build.js in the jquery package and on gruntfile.js
 */
({
    baseUrl: "src",
    name: "jstree",
    out: "dist/jstree.js",
    // We have multiple minify steps
    optimize: "none",
    // Avoid breaking semicolons inserted by r.js
    skipSemiColonInsertion: true,
    // otherwise define(xxx, function(){}) gets added
    skipModuleInsertion: true,
    wrap: {
        startFile: "src/intro.js",
        endFile:   "src/outro.js",
    },
    paths: {
        jquery: "empty:" 
    },
    include: [ "jstree.changed.js", "jstree.checkbox.js", "jstree.conditionalselect.js", "jstree.contextmenu.js", "jstree.dnd.js", "jstree.massload.js", "jstree.search.js", "jstree.sort.js", "jstree.state.js", "jstree.types.js", "jstree.unique.js", "jstree.wholerow.js", "vakata-jstree.js" ],
    rawText: {},
    /**
     * @param {String} name
     * @param {String} path
     * @param {String} contents The contents to be written (including their AMD wrappers)
     */
    onBuildWrite: function( name, path, contents ) {
        contents = contents.replace(/\s*if\(\$\.jstree\.plugins\.[a-z]+\)\s*\{\s*return;\s*\}/ig, '');
        contents = contents.replace(/\/\*globals[^\/]+\//ig, '');
        contents = contents.replace(/\(function \(factory[\s\S]*?undefined\s*\)[^\n]+/mig, '');
        contents = contents.replace(/\}\)\);/g, '');
        contents = contents.replace(/\s*("|')use strict("|');/g, '');
        contents = contents.replace(/\s*return \$\.fn\.jstree;/g, '');
        return contents;
    }
})

